# EENG20004 C for Embedded Systems virtual machine

A Vagrantfile to set up a CentOS 6 VM, install a desktop and Code Composer Studio version 5.5 to allow use of Texas Instruments simulator.

## Prerequisites

You'll need VirtualBox and Vagrant installed. MVB lab machines have these already.

## Installation

The easiest method is to use Git to clone this repository to a directory of your choice:

```bash
cd $HOME/your/chosen/directory  # Needs to exist before running this command
git clone https://gitlab.com/uob-eng/vms/eeng20004.git ./
```

Alternatively, download the Vagrantfile and save it to a location of your choice.

## Usage

```bash
cd $HOME/your/chosen/directory  # If not done above
vagrant up
```

When the machine GUI is loaded, log in as `vagrant` (password `vagrant`), and launch CCS from the Applications menu, under Development. The directory `/home/vagrant` on the VM corresponds to the directory on the host machine you chose earlier, so you'll find your CCS projects there.

The rest of the VM is housed elsewhere on your system, and if you change computers, you'll need to run `vagrant up` on the new machine as the VM does not move with you. In the labs, your home directory is available on all machines, so CCS project files should be in the same place when you enter the new VM.

You probably want to change the resolution of the VM display to make the window bigger (System menu, Preferences, Display). Ignore any messages about updating the VirtualBox additions to the VM - the installed version should work fine.

## Getting help

For help with Code Composer Studio, check with TAs or the unit director.

For help with getting the VM up and running, again start with TAs, but for lab machines you can also ask IT Services: [http://www.bristol.ac.uk/it-services/contacts/]().

The laptop clinic (linked from the IT Services contacts page) may be able to help you with running this VM on your own device.
